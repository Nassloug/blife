/!\ pour pas pourrir les sources, oubliez pas de passer l'encodage d'eclipse en utf8:

Windows > Preferences > General > Content Types, dans le champ Update mettre 'UTF-8' et appliquer pour chaque format de fichier.

Windows > Preferences > General > Workspace, mettre "Text file encoding" à "Other : UTF-8".

# Blife

![TOYS](http://static.fjcdn.com/pictures/Tibetan_6a1a95_1639835.jpg)

## Eclipse
WTP : http://download.eclipse.org/webtools/repository/indigo/

* Web Tools Plateform 3.3.2 

  * JavaScript Development Tools

  * Web Page Editor

# Test du projet en local 

Pour lancer le projet sur la machine du développeur et visiter les pages web sur http://localhost:8080/

modifier le pom pour ajouter jetty-maven-plugin :
    
    <project>

      ...

      <build>

        ...

        <plugins>

          ...

          <plugin>

            <groupId>org.eclipse.jetty</groupId>

            <artifactId>jetty-maven-plugin</artifactId>

            <version>9.3.0.M1</version>

          </plugin>

        </plugins>

      </build>

    <project>

Pour lancer un serveur local

    mvn jetty:run
    
# Création du projet depuis rien
## Maven
    
    mvn archetype:generate -DarchetypeGroupId=org.apache.maven.archetypes -DgroupId=fr.iutinfo -DartifactId=jersey-skeleton

# Eclipse

- plugin maven m2e depuis http://download.eclipse.org/releases/indigo/

- import existing maven project

// récupération de l'élément canvas sur lequel faire les dessins
var canvas = document.createElement("canvas");
//faible résolution pour éviter les ralentissements
canvas.width=720;canvas.height=405;

// récupération du contexte graphique
var ctx=canvas.getContext('2d');
var screen=window.screen;
var width = canvas.width;
var height = canvas.height;

//options de la matrice
var police=8;
var ySpacement=height/8;//100 à 200
var fallSpeed=20;//1 à 100
var fallSpace=1;//police*1.1;

//la matrice
var matrix = Array(Math.floor(height/police*2)).join(0).split('');
var matrix2 = Array(Math.floor(height/police*2)).join(0).split('');

//couleurs
var color='#A07';
var backgroundColor='rgba(0,0,0,0.25)';

//intervalles
var matrixCycle;var matrixCycleOn=false;
var rainbowCycle;var rainbowCycleOn=false;
var randomPoliceCycle;var randomPoliceCycleOn=false;

//compteurs
var rainbowIteration=0;
var whatIsInTheMatrix='l';
for(var i=0;i<matrix.length;i++){
	matrix[i]=height+ySpacement*height;
	matrix2[i]=0;
}
var draw=function(){
	ctx.fillStyle=backgroundColor;
	ctx.fillRect(0,0,width,height);
	ctx.font = police+'pt Arial';
	ctx.fillStyle=color;
	matrix.map(function(y, index){
		//texte affiché
		text = whatIsInTheMatrix;//Math.ceil(Math.random()*2-1);
		x = (index * police);
		if(matrix2[index]<1)text='\u266a';
		canvas.getContext('2d').fillText(text, x, y);
		if(y >  height+Math.random()*(ySpacement*height)){
			matrix[index]=0;
		}else{
			if(matrix2[index]==0)matrix2[index]=5*Math.random();
			matrix[index] = y +matrix2[index];
		}
	});
	$('body').css({'background-image':"url(" + canvas.toDataURL("image/jpeg")+ ")" });
};
var randomColor=function(){
	if(Math.random()<0.3){
		backgroundColor='rgba(16,0,0,0)';
		color='#F00';
	}else if(Math.random()<0.3){
		backgroundColor='rgba(0,16,0,0)';
		color='#0F0';
	}else{
		backgroundColor='rgba(0,0,16,0)';
		color='#00F';
	}
};
var setText=function(txt){whatIsInTheMatrix=txt;};
var toggleRainbowCycle=function(cycle){
	if(cycle==undefined)cycle=10;
	if(rainbowCycleOn==false){
		rainbowCycleOn=true;
		rainbowCycle=setInterval((function(){
			rainbowIteration+=30;
			var red=0;var green=0;var blue=0;
			if(rainbowIteration>1530)rainbowIteration=0;
			
			if(rainbowIteration>=0&&rainbowIteration<255)red=255;
			if(rainbowIteration>=255&&rainbowIteration<510)red=510-rainbowIteration;
			if(rainbowIteration>=510&&rainbowIteration<1020)red=0;
			if(rainbowIteration>=1020&&rainbowIteration<1275)red=rainbowIteration-1020;
			if(rainbowIteration>=1275&&rainbowIteration<=1530)red=255;
			
			if(rainbowIteration>=0&&rainbowIteration<255)green=rainbowIteration;
			if(rainbowIteration>=255&&rainbowIteration<765)green=255;
			if(rainbowIteration>=765&&rainbowIteration<1020)green=1020-rainbowIteration;
			if(rainbowIteration>=1020&&rainbowIteration<1530)green=0;
			
			if(rainbowIteration>=0&&rainbowIteration<510)blue=0;
			if(rainbowIteration>=510&&rainbowIteration<765)blue=rainbowIteration-510;
			if(rainbowIteration>=765&&rainbowIteration<1275)blue=255;
			if(rainbowIteration>=1275&&rainbowIteration<1530)blue=1530-rainbowIteration;
			color='rgb('+red+','+green+','+blue+')';
		}), cycle);
	}else{
		rainbowCycleOn=false;
		clearInterval(rainbowCycle);
	}
}
var toggleRandomPoliceCycle=function(cycle){
	if(cycle==undefined)cycle=100;
	if(randomPoliceCycleOn==false){
		randomPoliceCycleOn=true;
		randomPoliceCycle=setInterval((function(){
			police=8+Math.random()*8;
		}), cycle);
	}else{
		randomPoliceCycleOn=false;
		clearInterval(randomPoliceCycle);
	}
}
var toggleMatrixCycle=function(){
	if(matrixCycleOn==false){
		matrixCycleOn=true;
		matrixCycle=setInterval(draw,100/fallSpeed);
		toggleRainbowCycle();
		toggleRandomPoliceCycle();
	}else{
		toggleRainbowCycle();
		toggleRandomPoliceCycle();
		matrixCycleOn=false;
		clearInterval(matrixCycle);
	}
}
toggleMatrixCycle();
undefined;
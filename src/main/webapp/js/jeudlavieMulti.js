var Foxy = new Image();
Foxy.src = 'images/Foxy.png';
Foxy.onload = function() {
    console.info("Foxy loaded !");

};

var Bloby = new Image();
Bloby.src = 'images/Bloby.png';
Bloby.onload = function() {
    console.info("Bloby loaded !");

};

var Lapinou = new Image();
Lapinou.src = 'images/Lapinou.png';
Lapinou.onload = function() {
    console.info("Lapinou loaded !");

};
    
var Tichatgri = new Image();
Tichatgri.src = 'images/Tichatgri.png';
Tichatgri.onload = function() {
    console.info("Tichatgri loaded !");

};

var taille;
var monde;
var mondeTMP;
var nbEquipe;
var gameLoop;
var gameOn=false;

var canv = document.getElementById("screen");
var gfx = canv.getContext("2d");
canv.width=700;canv.height=700;
canv.addEventListener("click", (function(event){
	var x;var y;
	if(event.pageX||e.pageY){x=event.pageX;y=event.pageY;}else{
		x=event.clientX + document.body.scrollLeft + document.documentElement.scrollLeft; 
		y=event.clientY + document.body.scrollTop + document.documentElement.scrollTop; 
	} 
	x-=canv.offsetLeft;
	y-=canv.offsetTop;
	/**
	console.log("taille du tableau : "+monde.length+" "+monde[0].length);
	console.log("taille des cases : "+(canv.height/monde.length));
	console.log("X souris : "+x);
	console.log("Y souris : "+y);
	console.log("Y tableau : "+(Math.floor(y/(canv.height/monde.length))));
	console.log("X tableau : "+(Math.floor(x/(canv.width/monde[0].length))));
	**/
	if((Math.floor(y/(canv.height/monde.length)))<1||
			(Math.floor(x/(canv.width/monde[0].length)))<1||
			(Math.floor(y/(canv.height/monde.length)))>=monde.length-1||
			(Math.floor(x/(canv.width/monde[0].length)))>=monde[0].length-1){
		//alert("Hors limites !");
	}else{
		if((team==1&&
			(Math.floor(y/(canv.height/monde.length)))>=0&&(Math.floor(y/(canv.height/monde.length)))<=13&&
			(Math.floor(x/(canv.width/monde[0].length)))>=0&&(Math.floor(x/(canv.width/monde[0].length)))<=13)||
			(team==2&&
			(Math.floor(y/(canv.height/monde.length)))>=0&&(Math.floor(y/(canv.height/monde.length)))<=13&&
			(Math.floor(x/(canv.width/monde[0].length)))>=14&&(Math.floor(x/(canv.width/monde[0].length)))<=27)||
			(team==3&&
			(Math.floor(y/(canv.height/monde.length)))>=14&&(Math.floor(y/(canv.height/monde.length)))<=27&&
			(Math.floor(x/(canv.width/monde[0].length)))>=0&&(Math.floor(x/(canv.width/monde[0].length)))<=13)||
			(team==4&&
			(Math.floor(y/(canv.height/monde.length)))>=14&&(Math.floor(y/(canv.height/monde.length)))<=27&&
			(Math.floor(x/(canv.width/monde[0].length)))>=14&&(Math.floor(x/(canv.width/monde[0].length)))<=27)){
					monde[(Math.floor(y/(canv.height/monde.length)))][(Math.floor(x/(canv.width/monde[0].length)))]=team;
			$.ajax({
				type : 'POST',
				contentType : 'application/json',
				url : "v1/games/updateGame",
				dataType : "text",
				data : j1+":"+(Math.floor(y/(canv.height/monde.length)))+":"+(Math.floor(x/(canv.width/monde[0].length)))+":"+team,
				success : function(data, textStatus, jqXHR) {
					//alert(data);
				},
				error : function(jqXHR, textStatus, errorThrown) {
					console.log(jqXHR);
					console.log(textStatus);
					console.log(errorThrown);
					//alert('erreur');
				}
			});
			affiche();
		}
	}
}));
var mondeVide = function(nbLig, nbCol, value){
  var t= [];
  for(var i=0;i<nbLig;i++){
	t.push([]);
	for(var j=0;j<nbCol;j++){
		t[i].push(value);
	}
  }
  return t;
};

var affiche=function(){
	var tailleCase=(canv.height/monde.length);
	gfx.clearRect(tailleCase,tailleCase,canv.width-2*tailleCase,canv.height-2*tailleCase);
	gfx.fillStyle="rgba(255,255,255,0.5)";
	//dessin du fond transparent blanc
	gfx.fillRect(tailleCase,tailleCase,canv.width-2*tailleCase,canv.height-2*tailleCase);
	//dessin des bordures
	if(gameOn==false){
		gfx.fillRect((canv.width/2)-tailleCase/2,tailleCase,tailleCase,canv.height-2*tailleCase);
		gfx.fillRect(tailleCase,(canv.height/2)-tailleCase/2,canv.width-2*tailleCase,tailleCase);
		gfx.clearRect((canv.width/2)-tailleCase/2,(canv.height/2)-tailleCase/2,tailleCase,tailleCase);
		gfx.fillRect((canv.width/2)-tailleCase/2,(canv.height/2)-tailleCase/2,tailleCase,tailleCase);gfx.fillRect((canv.width/2)-tailleCase/2,(canv.height/2)-tailleCase/2,tailleCase,tailleCase);
	}
	for(var i  =1;i<monde.length-1;i++){
		for(var j=1;j<monde.length-1;j++){
			if(monde[i][j]===1){
				//gfx.fillStyle="#FF0000";
				gfx.drawImage(Foxy,(canv.width/taille)*j,(canv.height/taille)*i,canv.width/taille,canv.height/taille);
			}else if(monde[i][j]===2){
				//gfx.fillStyle="#0000FF";
				gfx.drawImage(Bloby,(canv.width/taille)*j,(canv.height/taille)*i,canv.width/taille,canv.height/taille);
			}else if(monde[i][j]===3){
				//gfx.fillStyle="#00C10D";
				//gfx.fillRect((canv.width/taille)*j,(canv.height/taille)*i,canv.width/taille,canv.height/taille);
				gfx.drawImage(Lapinou,(canv.width/taille)*j,(canv.height/taille)*i,canv.width/taille,canv.height/taille);
			}else if(monde[i][j]===4){
				//gfx.fillStyle="#FF00FF";
				//gfx.fillRect((canv.width/taille)*j,(canv.height/taille)*i,canv.width/taille,canv.height/taille);
				gfx.drawImage(Tichatgri,(canv.width/taille)*j,(canv.height/taille)*i,canv.width/taille,canv.height/taille);
			}
			
		}
	}
	if(gameOn==false){gfx.fillRect(tailleCase,tailleCase,canv.width-2*tailleCase,canv.height-2*tailleCase);}
	$("#nbRouge").html('Joueur 1 (Foxy) : '+colorCount(1));
	$("#nbBleu").html('Joueur 2 (Bloby) : '+colorCount(2));
	$("#nbVert").html('Joueur 3 (Lapinou) : '+colorCount(3));
	$("#nbMagenta").html('Joueur 4 (Tichatgri) : '+colorCount(4));
}

var voisinsVivante = function(lig, col,equipe) {
    var t = [];
    if(monde[lig-1][col-1]===equipe){t.push(monde[lig-1][col-1])};
    if(monde[lig-1][col]===equipe){t.push(monde[lig-1][col])};
    if(monde[lig-1][col+1]===equipe){t.push(monde[lig-1][col+1])};
    if(monde[lig][col-1]===equipe){t.push(monde[lig][col-1])};
    if(monde[lig+1][col-1]===equipe){t.push(monde[lig+1][col-1])};
    if(monde[lig+1][col+1]===equipe){t.push(monde[lig+1][col+1])};
    if(monde[lig][col+1]===equipe){t.push(monde[lig][col+1])};
    if(monde[lig+1][col]===equipe){t.push(monde[lig+1][col])};
    return t;
};

var voisins = function(lig, col,equipe) {
    var t = [];
    if(monde[lig-1][col-1]!=0){t.push(monde[lig-1][col-1])};
    if(monde[lig-1][col]!=0){t.push(monde[lig-1][col])};
    if(monde[lig-1][col+1]!=0){t.push(monde[lig-1][col+1])};
    if(monde[lig][col-1]!=0){t.push(monde[lig][col-1])};
    if(monde[lig+1][col-1]!=0){t.push(monde[lig+1][col-1])};
    if(monde[lig+1][col+1]!=0){t.push(monde[lig+1][col+1])};
    if(monde[lig][col+1]!=0){t.push(monde[lig][col+1])};
    if(monde[lig+1][col]!=0){t.push(monde[lig+1][col])};
    return t;
};

var voisinsMorte = function(lig, col) {
    var t = [0,0,0,0,0];
    if(monde[lig-1][col-1]!=0){t[monde[lig-1][col-1]]++;};
    if(monde[lig-1][col]!=0){t[monde[lig-1][col]]++;};
    if(monde[lig-1][col+1]!=0){t[monde[lig-1][col+1]]++;};
    if(monde[lig][col-1]!=0){t[monde[lig][col-1]]++;};
    if(monde[lig+1][col-1]!=0){t[monde[lig+1][col-1]]++;};
    if(monde[lig+1][col+1]!=0){t[monde[lig+1][col+1]]++;};
    if(monde[lig][col+1]!=0){t[monde[lig][col+1]]++;};
    if(monde[lig+1][col]!=0){t[monde[lig+1][col]]++;};
    
    var index=0;
    for(var i=0;i<t.length;i++){
	if(t[i]>t[index] && t[i]>2){index=i;}
    }

    return index;
};

var evolution = function(vivante, nbVoisinsTeam,nbVoisins) {
   if(vivante!=0){
	if(nbVoisins>5){
		return 0;
	}else if(nbVoisinsTeam>2){
		return vivante;
	}else{return 0;}
   }else{
	if(nbVoisins>3){
		return vivante;
	}else{return 0;}
   }
}

var evolutionMorte = function(lig,col) {
	return voisinsMorte(lig,col);
}
    
var simule = function() {
    for(var i =1;i<monde.length-1;i++){
	for(var j=1;j<monde[0].length-1;j++){
		if(monde[i][j]!=0){
			mondeTMP[i][j]=evolution(monde[i][j],voisinsVivante(i,j,monde[i][j]).length,voisins(i,j,monde[i][j]).length);
		}else{
			mondeTMP[i][j]=evolutionMorte(i,j);
		}
  	}
    }
    monde=mondeTMP;

    mondeTMP = mondeVide(taille,taille,0);
};


var toggleGame = function(cycle){
	if(cycle==undefined)cycle=750;
	if(gameOn==false){
		gameOn=true;
		gameLoop=setInterval(function() {affiche(monde); simule(); }, cycle);
	}else{
		gameOn=false;
		clearInterval(gameLoop);
		affiche();
	}
}

var initialisation = function(t,pourcentageVivant,nbTeam){
	if(nbTeam>4 || nbTeam<1){
		nbEquipe=1;
	}else{
		nbEquipe=nbTeam;
	}
	taille=t;
	monde = mondeVide(taille,taille,0);
	for(var i =1;i<monde.length-1;i++){
		for(var j=1;j<monde[0].length-1;j++){
			if(Math.floor((Math.random() * 100) + 1)<pourcentageVivant){
				monde[i][j]=Math.floor((Math.random() * nbEquipe) + 1);
			} 
		}
	}
	mondeTMP = mondeVide(taille,taille,0);

}

var vitalize = function(x,y,width,height,alivePercent,team){
	if(team<=4||team>=1){
		for(var i =x+1;i<=x+width;i++){
			for(var j=y+1;j<=y+height;j++){
				if(Math.floor((Math.random() * 100) + 1)<alivePercent){
					monde[i][j]=team;
				} 
			}
		}
		affiche();
	}
}

var kill = function(x,y,width,height,team){
	if(team<=4||team>=1&&x<=taille-2&&y<=taille-2&&x>=0&&y>=0&&x+width<=taille-2&&y+height<=taille-2){
		for(var i =x+1;i<=x+width;i++){
			for(var j=y+1;j<=y+height;j++){
				if(monde[i][j]==team){
					monde[i][j]=0;
				} 
			}
		}
		affiche();
	}
}

var update = function(host,user,userID){
	if(userID!=1){//joiner
		$.ajax({
			type : 'POST',
			contentType : 'application/json',
			url : "v1/games/getGame",
			dataType : "text",
			data : j1,
			success : function(data, textStatus, jqXHR) {
				if(data!=""){
					var t;
					for(var i=0;i<data.split(";").length-1;i++){
						t=data.split(";")[i];
						console.info(parseInt(t.split(":")[0])+""+parseInt(t.split(":")[1])+""+parseInt(t.split(":")[2]));
						monde[parseInt(t.split(":")[0])][parseInt(t.split(":")[1])]=parseInt(t.split(":")[2]);
					}
					affiche();
				}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR);
				console.log(textStatus);
				console.log(errorThrown);
				//alert('erreur');
			}
		});
	}else{//host
		$.ajax({
			type : 'POST',
			contentType : 'application/json',
			url : "v1/games/getGame",
			dataType : "text",
			data : j1,
			success : function(data, textStatus, jqXHR) {
				if(data!=""){
					var t;
					for(var i=0;i<data.split(";").length-1;i++){
						t=data.split(";")[i];
						console.info(parseInt(t.split(":")[0])+""+parseInt(t.split(":")[1])+""+parseInt(t.split(":")[2]));
						monde[parseInt(t.split(":")[0])][parseInt(t.split(":")[1])]=parseInt(t.split(":")[2]);
					}
					affiche();
				}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR);
				console.log(textStatus);
				console.log(errorThrown);
				//alert('erreur');
			}
		});
	}
}

var colorCount = function(equipe){
	var count=0;
	for(var i =1;i<monde.length-1;i++){
		for(var j=1;j<monde[0].length-1;j++){
			if(monde[i][j]===equipe){
				count++;
			}
	  	}
	}
	return count;
}
initialisation(2*13+3,0,4);
affiche();
setInterval(update,500);
undefined;


function filter(idCookie){
	$.ajax({
		type : 'POST',
		contentType : 'application/json',
		url : "v1/connect/check",
		dataType : "json",
		data : JSON.stringify({
			"id" : idCookie
		}),
		success : function(data, textStatus, jqXHR) {
			if(!data.success){//si l'user est un visiteur
				window.location.replace("connexion.html");
			}
		},
		error : function(jqXHR, textStatus, errorThrown) {
			//alert('postUser is connected error: ' + textStatus);
		}
	});
}
filter(readCookie("id"));
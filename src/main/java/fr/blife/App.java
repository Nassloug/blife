package fr.blife;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.glassfish.jersey.filter.LoggingFilter;
import org.skife.jdbi.v2.DBI;
import org.sqlite.SQLiteDataSource;

import fr.blife.ressources.*;
import fr.blife.utils.InitDB;

@ApplicationPath("/v1/")
public class App extends Application {
	public static SQLiteDataSource ds;
	public static DBI dbi;
	
    @Override
    public Set<Class<?>> getClasses() {
		ds = new SQLiteDataSource();
		ds.setUrl("jdbc:sqlite:bdd");
		
		dbi = new DBI(ds);
    	InitDB.up(dbi);
    	Set<Class<?>> s = new HashSet<Class<?>>();
    	s.add(InscriptionRessource.class);
    	s.add(LoggingFilter.class);
    	s.add(ConnexionRessource.class);
    	s.add(ModifierProfilRessource.class);
    	s.add(UserRessource.class);
    	s.add(SuppressionUserRessource.class);
    	s.add(PartieRessource.class);
    	return s;
    }    
}

package fr.blife.dao;

import org.skife.jdbi.v2.sqlobject.SqlUpdate;

public interface UserDao{
	@SqlUpdate("CREATE TABLE users (pseudo text primary key,mdp text);")
	void createTableUsers();
	@SqlUpdate("INSERT INTO users (pseudo, mdp) values ('root','root');")
	void addRootAdmin();
	void close();
}

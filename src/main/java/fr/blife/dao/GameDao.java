package fr.blife.dao;

import org.skife.jdbi.v2.sqlobject.SqlUpdate;

public interface GameDao{
	@SqlUpdate("DROP TABLE games;")
	void dropTableGames();
	@SqlUpdate("CREATE TABLE games (nom text,j1 text primary key,j2 text,j3 text,j4 text,plateau text);")
	void createTableGames();
	void close();
}

package fr.blife.ressources;

import java.sql.Connection;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import fr.blife.model.FeedBack;
import fr.blife.model.User;
import fr.blife.utils.Requests;
import fr.blife.utils.SQLiteConnection;

@Path("/deleteAdmin")
@Produces(MediaType.APPLICATION_JSON)
public class SupressionAdminRessource {
	
	@POST 
	public FeedBack connect(User user) {
		Connection c = SQLiteConnection.getConnection();
		Requests r = new Requests(c);

		if(r.supprimerCompte(user)){
			return new FeedBack(true, "Utilisateur supprimé");
		}
		else{ 
			return new FeedBack(true, "Pseudo inconnu");
		}
	}
}

package fr.blife.ressources;

import java.sql.Connection;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import fr.blife.model.Cellule;
import fr.blife.model.Coord;
import fr.blife.model.FeedBack;
import fr.blife.model.Game;
import fr.blife.model.Plateau;
import fr.blife.utils.Requests;
import fr.blife.utils.Ressources;
import fr.blife.utils.SQLiteConnection;

@Path("/games")
@Produces(MediaType.APPLICATION_JSON)
public class PartieRessource {
	@POST
	@Path("/createGame")
	public FeedBack createGame(Game game) {
		Connection c = SQLiteConnection.getConnection();
		Requests r = new Requests(c);
		List<String> nomPartiesEnCours = r.select("games");
		if(nomPartiesEnCours.contains(game.getP1()))return new FeedBack(false, "Erreur, vous avez déjà créée une partie");
		r.insertGame(game);
		Ressources.addGame(game.getP1(), new Plateau(27, 27, 4));;
		return new FeedBack(true, "Partie créée ! ");
		
	}
	@POST
	@Path("/updateGame")
	public FeedBack updateGame(String pseudoTeam) {
		String pseudo=pseudoTeam.split(":")[0];
		int x = Integer.parseInt(pseudoTeam.split(":")[1]);int y = Integer.parseInt(pseudoTeam.split(":")[2]);
		int team = Integer.parseInt(pseudoTeam.split(":")[3]);
		Ressources.getGames().get(pseudo).getCellule(new Coord(x,y)).setEquipe(team);
		return new FeedBack(true,"Partie mise à jour !");
	}
	@POST
	@Path("/joinGame")
	public String joinGame(Game game) {
		Connection c = SQLiteConnection.getConnection();
		Requests r = new Requests(c);
		return ""+r.joinGame(game.getP1(),game.getP2());
	}
	@GET
	@Path("/listGames")
	public String listGames() {
		Connection c = SQLiteConnection.getConnection();
		Requests r = new Requests(c);

		return r.getGames();
	}
	@POST
	@Path("/getGame")
	public String getGame(String j1) {
		String result="";
		System.out.println(j1);
		for(Cellule c:Ressources.getGames().get(j1).getCellules()){
			result+=c.getX()+":"+c.getY()+":"+c.getEquipe()+";";
		}
		return result;
	}
}

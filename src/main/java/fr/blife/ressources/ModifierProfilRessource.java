package fr.blife.ressources;

import java.sql.Connection;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import fr.blife.model.FeedBack;
import fr.blife.model.User;
import fr.blife.utils.Requests;
import fr.blife.utils.SQLiteConnection;

@Path("/modifprofil")
@Produces(MediaType.APPLICATION_JSON)
public class ModifierProfilRessource {

	@POST
	public FeedBack modifierProfil(User user) {
		Connection c = SQLiteConnection.getConnection();
		Requests r = new Requests(c);
		
		 if(!user.getMdp().equals("")) {
			r.modifierMdp(user);
			return new FeedBack(true, "Votre mot de passe a été modifié");
		 }
		else {
			return new FeedBack(false, "Veuillez remplir au moins un champ");
		}
	}
}

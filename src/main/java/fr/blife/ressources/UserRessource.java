package fr.blife.ressources;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import fr.blife.model.Cookie;
import fr.blife.model.User;
import fr.blife.utils.Ressources;

@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
public class UserRessource {
	@POST
	public User getUser(Cookie c) {
		return Ressources.getUser(c.getId());
	}
}

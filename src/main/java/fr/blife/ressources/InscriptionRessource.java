package fr.blife.ressources;

import java.sql.Connection;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import fr.blife.model.FeedBack;
import fr.blife.model.User;
import fr.blife.utils.Requests;
import fr.blife.utils.SQLiteConnection;

@Path("/register")
@Produces(MediaType.APPLICATION_JSON)
public class InscriptionRessource {
	@POST
	public FeedBack register(User user) {
		Connection c = SQLiteConnection.getConnection();
		Requests r = new Requests(c);
		
		if(user.getPseudo().length()>7)return new FeedBack(false, "Erreur, pseudo trop long");
		if(user.getPseudo().length()<3)return new FeedBack(false, "Erreur, pseudo trop court");
		List<String> pseudos = r.select("users");
		if(pseudos.contains(user.getPseudo()))return new FeedBack(false, "Erreur, pseudo déjà utilisé");

		r.insertUser(user);
		return new FeedBack(true, "Inscription réussie, redirection vers la page de connexion dans 3 secondes...");
		
	}
}

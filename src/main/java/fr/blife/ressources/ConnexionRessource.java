package fr.blife.ressources;

import java.util.Map.Entry;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import fr.blife.model.Cookie;
import fr.blife.model.FeedBack;
import fr.blife.model.User;
import fr.blife.utils.Requests;
import fr.blife.utils.Ressources;
import fr.blife.utils.UniqueIdGenerator;

@Path("/connect")
@Produces(MediaType.APPLICATION_JSON)
public class ConnexionRessource {
	@POST
	public Cookie connect(User user) {
		Requests r = new Requests();
		if(!r.idExist(user)) {
			return new Cookie(-1);
		} else {
			int id = UniqueIdGenerator.getUniqueId();
			User u = r.getUser(user.getPseudo());
			Ressources.addUser(id, u);
			for(Entry<Integer, User> e : Ressources.getUsersLogin().entrySet()) {
				System.out.println(e.getKey() + "->" + e.getValue());
			}
			return new Cookie(id);
		}
	}
	
	@POST
	@Path("/check")
	public FeedBack isConnected(Cookie c) {
		if(Ressources.isConnected(c)) {
			return new FeedBack(true, "utilisateur connecté");
		} else {
			return new FeedBack(false, "utilisateur non connecté");
		}
	}
	
	
	@POST
	@Path("/logout")
	public FeedBack logout(Cookie c) {
		if(Ressources.isConnected(c)) {
			Ressources.removeUser(c.getId());
			return new FeedBack(true, "utilisateur déconnecté");
		}else{
			return new FeedBack(false, "utilisateur non connecté");
		}
	}
}

package fr.blife.utils;

import java.util.HashMap;

import fr.blife.model.Cookie;
import fr.blife.model.Plateau;
import fr.blife.model.User;

public class Ressources {

	private static HashMap<Integer,User>usersLogin;
	private static HashMap<String,Plateau>games;
	public static HashMap<Integer, User> getUsersLogin() {
		if(usersLogin == null) usersLogin = new HashMap<Integer, User>();
		return usersLogin;
	}
	
	public static HashMap<String, Plateau> getGames() {
		if(games == null) games = new HashMap<String, Plateau>();
		return games;
	}
	
	public static User getUser(Integer uniqueId) {
		return getUsersLogin().get(uniqueId);
	}
	
	public static Integer getID(User u) {
		for(Integer i:getUsersLogin().keySet()){
			if(getUser(i).getPseudo().equals(u.getPseudo()))return i;
		}
		return -1;
	}
	
	public static void addUser(Integer uniqueId, User u) {
		getUsersLogin().put(uniqueId, u);
	}
	
	public static void addGame(String pseudo, Plateau p) {
		getGames().put(pseudo, p);
	}
	
	public static void removeUser(Integer uniqueId) {
		System.out.println(listUsers());
		getUsersLogin().remove(uniqueId);
		System.out.println(listUsers());
	}

	public static boolean isConnected(Cookie c) {
		if(c == null) return false;
		return getUsersLogin().containsKey(c.getId());
	}
	public static String listUsers(){
		String result="";
		for(User u:getUsersLogin().values()){
			result+=u+" : "+getID(u)+"\n";
		}
		return result;
	}
	public static void setGameCase(String pseudo,int x,int y,int value){
		games.get(pseudo).getCellule(x, y).setEquipe(value);
	}
}

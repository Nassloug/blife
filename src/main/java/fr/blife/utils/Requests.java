package fr.blife.utils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import fr.blife.model.Game;
import fr.blife.model.User;
public class Requests {
	private Connection c;
	public Requests(Connection c) {
		this.c = SQLiteConnection.getConnection();
	}
	public Requests() {
		this.c = SQLiteConnection.getConnection();
	}
	public void insertUser(User u) {
		PreparedStatement stmt = null;
		try {
			String sql = "INSERT INTO users(pseudo,mdp) VALUES(?, ?)";
			stmt = c.prepareStatement(sql);
			stmt.setString(1, u.getPseudo());
			stmt.setString(2, u.getMdp());
			stmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		} finally {
			try {
				stmt.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	public List<String> select(String table) {
		List<String> tmp = new LinkedList<String>();
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = c.createStatement();
			rs = stmt.executeQuery("SELECT * FROM " + table);
			String primaryKey="";
			if(table=="users")primaryKey="pseudo";
			if(table=="games")primaryKey="j1";
			while (rs.next()){
				tmp.add(rs.getString(primaryKey));
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		} finally {
			try {
				rs.close();
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return tmp;
	}
	public boolean idExist(User user) {
		PreparedStatement stmt = null;
		try {
			stmt = c.prepareStatement("select * from users where pseudo = ? and mdp = ?");
			stmt.setString(1, user.getPseudo());
			stmt.setString(2, user.getMdp());
			return stmt.executeQuery().next();
		} catch(Exception e) {
			e.printStackTrace();
			System.exit(0);
		} finally {
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
	public boolean supprimerCompte(User user) {
		Statement stmt = null;
		ResultSet rs = null;
		boolean tmp = false;
		try {
			stmt = c.createStatement();
			rs = stmt.executeQuery("SELECT * FROM users WHERE pseudo='"+user.getPseudo()+"'");
			if(rs.next()) {
				tmp = true;
				stmt.executeUpdate("DELETE FROM users WHERE pseudo='" + user.getPseudo()+"'");
			}
		} catch(Exception e) {
			e.printStackTrace();
			System.exit(0);
		} finally {
			try {
				rs.close();
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if(tmp)Ressources.removeUser(Ressources.getID(user));
		return tmp;
	}
	public void modifierMdp(User user) {
		Statement stmt = null;
		try {
			stmt = c.createStatement();
			stmt.executeUpdate("UPDATE users SET mdp='"+user.getMdp()+"' WHERE pseudo='"+user.getPseudo()+"'");
		} catch(Exception e) {
			e.printStackTrace();
			System.exit(0);
		} finally {
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	public User getUser(String pseudo) {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			stmt = c.prepareStatement("select * from users where pseudo = ?;");
			stmt.setString(1, pseudo);
			rs = stmt.executeQuery();
			if(rs.next())
				return new User(rs.getString("pseudo"), rs.getString("mdp"));
		} catch(Exception e) {
			e.printStackTrace();
			System.exit(0);
		} finally {
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	public void insertGame(Game g) {
		PreparedStatement stmt = null;
		try {//nom text primary key,j1 text,j2 text,j3 text,j4 text,plateau text
			String sql = "INSERT INTO games(nom, j1,j2,j3,j4, plateau) VALUES(?, ?,?,?,?, ?)";
			stmt = c.prepareStatement(sql);
			stmt.setString(1, g.getName());
			stmt.setString(2, g.getP1());
			stmt.setString(3, g.getP2());
			stmt.setString(4, g.getP3());
			stmt.setString(5, g.getP4());
			stmt.setString(6, g.getBoard());
			stmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		} finally {
			try {
				stmt.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	public boolean deleteGame(String j1) {
		Statement stmt = null;
		ResultSet rs = null;
		boolean result=false;
		try {
			stmt = c.createStatement();
			rs = stmt.executeQuery("DELETE FROM games WHERE p1='"+j1+"'");
			result=true;
		} catch(Exception e) {
			e.printStackTrace();
			System.exit(0);
		} finally {
			try {
				rs.close();
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	public boolean majGame(String j1,Game newGame) {
		Statement stmt = null;
		ResultSet rs = null;
		boolean tmp = false;
		try {
			stmt = c.createStatement();
			rs = stmt.executeQuery("SELECT * FROM games WHERE j1='"+j1+"'");
			if(rs.next()) {
				tmp = true;
				if(!newGame.getName().equals(""))stmt.executeUpdate("UPDATE games SET nom = '"+newGame.getName()+"' WHERE j1='"+j1+"'");
				if(!newGame.getP1().equals(""))stmt.executeUpdate("UPDATE games SET j1 = '"+newGame.getP1()+"' WHERE j1='"+j1+"'");
				if((!newGame.getP2().equals("")||!newGame.getP2().equals("Aucun"))&&
						!newGame.getP2().equals(rs.getString("j1"))&&
						!newGame.getP2().equals(rs.getString("j2"))&&
						!newGame.getP2().equals(rs.getString("j3"))&&
						!newGame.getP2().equals(rs.getString("j4"))){//cas d'un joiner qui n'est pas déjà dans la partie
					if(!newGame.getP2().equals("")&&(rs.getString("j2").equals("")||rs.getString("j2").equals("Aucun"))){
						stmt.executeUpdate("UPDATE games SET j2 = '"+newGame.getP2()+"' WHERE j1='"+j1+"'");
					}else if(!newGame.getP2().equals("")&&(rs.getString("j3").equals("")||rs.getString("j3").equals("Aucun"))){
						stmt.executeUpdate("UPDATE games SET j3 = '"+newGame.getP2()+"' WHERE j1='"+j1+"'");
					}else if(!newGame.getP2().equals("")&&(rs.getString("j4").equals("")||rs.getString("j4").equals("Aucun"))){
						stmt.executeUpdate("UPDATE games SET j4 = '"+newGame.getP2()+"' WHERE j1='"+j1+"'");
					}
				}
				if(!newGame.getBoard().equals(""))stmt.executeUpdate("UPDATE games SET plateau = '"+newGame.getBoard()+"' WHERE j1='"+j1+"'");
			}
		} catch(Exception e) {
			e.printStackTrace();
			System.exit(0);
		} finally {
			try {
				rs.close();
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return tmp;
	}
	public int joinGame(String host,String join) {
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = c.createStatement();
			rs = stmt.executeQuery("SELECT * FROM games WHERE j1='"+host+"'");
			if(rs.next()) {
				if((!join.equals("")||!join.equals("Aucun"))&&
						!join.equals(rs.getString("j1"))&&
						!join.equals(rs.getString("j2"))&&
						!join.equals(rs.getString("j3"))&&
						!join.equals(rs.getString("j4"))){//cas d'un joiner qui n'est pas déjà dans la partie
					if(!join.equals("")&&(rs.getString("j2").equals("")||rs.getString("j2").equals("Aucun"))){
						stmt.executeUpdate("UPDATE games SET j2 = '"+join+"' WHERE j1='"+host+"'");
						return 2;
					}else if(!join.equals("")&&(rs.getString("j3").equals("")||rs.getString("j3").equals("Aucun"))){
						stmt.executeUpdate("UPDATE games SET j3 = '"+join+"' WHERE j1='"+host+"'");
						return 3;
					}else if(!join.equals("")&&(rs.getString("j4").equals("")||rs.getString("j4").equals("Aucun"))){
						stmt.executeUpdate("UPDATE games SET j4 = '"+join+"' WHERE j1='"+host+"'");
						return 4;
					}
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
			System.exit(0);
		} finally {
			try {
				rs.close();
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return -1;
	}
	public String getGame(String j1){
		Statement stmt = null;
		ResultSet rs = null;
		String result="";
		try {
			stmt = c.createStatement();
			rs = stmt.executeQuery("SELECT * FROM games WHERE j1='"+j1+"'");
			while (rs.next()){
				result=	rs.getString("nom")+":"+rs.getString("j1")+":"+rs.getString("j2")+":"+rs.getString("j3")+":"+rs.getString("j4")+":"+rs.getString("plateau");
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		} finally {
			try {
				rs.close();
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	public String getGames(){
		List<Game> tmp = new LinkedList<Game>();
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = c.createStatement();
			rs = stmt.executeQuery("SELECT * FROM games");
			while (rs.next()){
					tmp.add(new Game(rs.getString("nom"),rs.getString("j1"),rs.getString("j2"),rs.getString("j3"),rs.getString("j4"),rs.getString("plateau")));
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		} finally {
			try {
				rs.close();
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		String result="<table>";
		for(Game g:tmp){
			result+="<tr ><td class=join_column><button onclick=\"location.href = 'JDLV.html?join="+g.getP1()+"';\" class=\"btn btn-primary\">Rejoindre la partie</button></td><td class=name_column>"+
		g.getName()+"</td><td class=p1_column>"+g.getP1()+"</td><td class=p2_column>"+g.getP2()+"</td><td class=p3_column>"+g.getP3()+"</td><td class=p4_column>"+g.getP4()+"</td><td class=spectate_column><button onclick=\"location.href = 'JDLV.html?spectate="+g.getP1()+"';\" class=\"btn btn-primary\">Regarder la partie</button></td></tr>";
		}
		return result+"</table>";
	}
	public boolean supprimerPartie(Game game) {
		Statement stmt = null;
		ResultSet rs = null;
		boolean tmp = false;
		try {
			stmt = c.createStatement();
			rs = stmt.executeQuery("SELECT * FROM games WHERE name='"+game.getName()+"'");
			if(rs.next()) {
				tmp = true;
				stmt.executeUpdate("DELETE FROM games WHERE name='" + game.getName()+"'");
			}
		} catch(Exception e) {
			e.printStackTrace();
			System.exit(0);
		} finally {
			try {
				rs.close();
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return tmp;
	}
}
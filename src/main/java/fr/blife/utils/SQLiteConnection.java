package fr.blife.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.sqlite.SQLiteDataSource;

import fr.blife.App;

public class SQLiteConnection {
	private static Connection c;
	private static SQLiteDataSource ds;

	private SQLiteConnection() {}


	public static Connection getConnection() {
		if(c == null)
			connect();

		return c;
	}

	private static void connect() {
		try {
			ds = App.ds;
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(ds.getUrl());
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("Impossible de se connecter a la base de donnée !");
			System.exit(0);
		} catch (ClassNotFoundException e) {
			System.err.println("Impossible de charger le driver ! (driver incorrect ou inexistant)");
			System.exit(0);
		}
		System.out.println("Connexion établie avec la base de donnée !");
	}

	public static void close() {
		try {
			c.close();
		} catch (SQLException e) {
			System.err.println("Impossible de fermer la connexion (connexion inexistante)");
			System.exit(0);
		}
	}
}

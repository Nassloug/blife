package fr.blife.utils;

import org.skife.jdbi.v2.DBI;

import fr.blife.dao.GameDao;
import fr.blife.dao.UserDao;

public class InitDB {
static boolean alreadyStarted=false;
	public static void up(DBI dbi) {
		if(alreadyStarted)return;
		UserDao daou = dbi.open(UserDao.class);
		try {
			daou.createTableUsers();
		} catch (Exception e){
			System.out.println("Tables utilisateurs déjà créées !");
		}
		try {
			daou.addRootAdmin();
		} catch (Exception e) {
			System.out.println("Administrateur déjà enregistré !");
		}
		GameDao daog = dbi.open(GameDao.class);
		try {
			daog.createTableGames();
		} catch (Exception e){
			daog.dropTableGames();
			daog.createTableGames();
		}
		alreadyStarted=true;
	}

}

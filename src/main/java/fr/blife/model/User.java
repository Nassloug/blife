package fr.blife.model;

public class User {
	private String pseudo, mdp;

	public User( String pseudo, String mdp) {
		this.setPseudo(pseudo);
		this.setMdp(mdp);
	}


	public User() {
		
	}
	
	 public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	
	public String getMdp() {
		return mdp;
	}

	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

	
	
	public String toString() {
		return pseudo;
	}
}
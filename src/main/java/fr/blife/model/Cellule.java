package fr.blife.model;

import java.util.ArrayList;

public class Cellule {
	private Coord coord;
	private int equipe;
	
	public Cellule(){
		this.equipe = 0;
	}
	
	public Cellule (int x,int y,int equipe){
		coord=new Coord(x, y);
		this.equipe=equipe;
	}
	
	public Coord getCoord(){
		return coord;
	}
	
	public int getX(){
		return coord.getX();
	}
	
	public int getY(){
		return coord.getY();
	}
	
	public void setCoord(int x,int y){
		coord.setX(x);
		coord.setY(y);
	}
	
	public void setCoord(Coord coord){
		this.coord = coord;
	}
	
	public int getEquipe(){
		return this.equipe;
	}
	
	public boolean isNear(Cellule cellule){
		return this.coord.isNear(cellule.getCoord());
	}
	
	public boolean is(Cellule cellule){
		return this.coord.equals(cellule.getCoord());
	}
	
	protected Cellule cloner(){
		Cellule newCell = new Cellule();
		newCell.setCoord(coord);
		newCell.setEquipe(equipe);
		return newCell;
	}
	
	public void setEquipe(int equipe){
		this.equipe=equipe;
	}
}

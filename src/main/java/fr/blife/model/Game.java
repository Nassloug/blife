package fr.blife.model;

import java.util.ArrayList;

public class Game {
	private String name,p1,p2,p3,p4,plateau;
	private Plateau board;
	static final int BOARD_SIZE = 50;

	public Game(){
		this.name = "NaN";
		this.p1 = "NaN";
		this.p2 = "NaN";
		this.p3 = "NaN";
		this.p4 = "NaN";
		this.plateau = "NaN";
		this.board = new Plateau(BOARD_SIZE,BOARD_SIZE,4);
	}
	
	public Game(String name, String p1, String p2, String p3, String p4,String plateau){
		this.name = name;
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
		this.p4 = p4;
		this.plateau = plateau;
		this.board = new Plateau(BOARD_SIZE,BOARD_SIZE,4);
		this.board.getMapFromString(plateau);
	}

	public String getName() {return name;}
	public void setName(String name) {this.name = name;}
	public String getP1() {return p1;}
	public void setP1(String p1) {this.p1 = p1;}
	public String getP2() {return p2;}
	public void setP2(String p2) {this.p2 = p2;}
	public String getP3() {return p3;}
	public void setP3(String p3) {this.p3 = p3;}
	public String getP4() {return p4;}
	public void setP4(String p4) {this.p4 = p4;}
	public String getBoard() {return plateau;}
	public void setBoard(String board) {this.plateau = board;}
	
	public String startGame(int nbTours){
		int winner;
		String playerWinner = "";
		Cellule newC;
		Cellule c;
		Plateau newBoard;
		ArrayList<Cellule> voisines = new ArrayList<Cellule>();
		int eq1;
		int eq2;
		int eq3;
		int eq4;
		for(int i=0; i<nbTours; i++){
			newBoard = new Plateau(board.getNbLignes(), board.getNbColonnes(), 4);
			for(int n=0; n<BOARD_SIZE;n++){
				for(int k=0; k<BOARD_SIZE;k++){
					c = new Cellule(n,k,0);
					eq1 = 0;
					eq2 = 0;
					eq3 = 0;
					eq4 = 0;
					voisines.clear();
					for(Cellule c2: board.getCellules()){
						if(c.isNear(c2)){
							voisines.add(c2);
						}
						else if(c.is(c2)){
							c = c2;
						}
					}
					if(voisines.size()<6 && ((c.getEquipe() != 0 && voisines.size()>=2) || voisines.size()>=3)){
						for(Cellule cell: voisines){
							if(cell.getEquipe()==1){
								eq1++;
							}
							else if(cell.getEquipe()==2){
								eq2++;
							}
							else if(cell.getEquipe()==3){
								eq3++;
							}
							else if(cell.getEquipe()==4){
								eq4++;
							}
						}
						newC = new Cellule();
						newC = c.cloner();
						newBoard.ajouterCellule(newC);
						if(eq1>=3){
							if(newC.getEquipe()!=1){
								newBoard.changeEquipe(newC,1);
							}
						}
						else if(eq2>=3){
							if(newC.getEquipe()!=2){
								newBoard.changeEquipe(newC,2);
							}
						}
						else if(eq3>=3){
							if(newC.getEquipe()!=3){
								newBoard.changeEquipe(newC,3);
							}
						}
						else if(eq4>=3){
							if(newC.getEquipe()!=4){
								newBoard.changeEquipe(newC,4);
							}
						}
					}
				}
			}
			board = newBoard;
			try{
				Thread.sleep(500);
			}
			catch(Exception e){
				e.getStackTrace();
			}
		}
		eq1 = board.getSizeTeam(1);
		eq2 = board.getSizeTeam(2);
		eq3 = board.getSizeTeam(3);
		eq4 = board.getSizeTeam(4);
		winner = Math.max(eq1, eq2);
		winner = Math.max(winner, eq3);
		winner = Math.max(winner, eq4);
		if(winner==eq1){
			playerWinner = p1;
		}
		else if(winner==eq2){
			playerWinner = p2;
		}
		else if(winner==eq3){
			playerWinner = p3;
		}
		else if(winner==eq4){
			playerWinner = p4;
		}
		return playerWinner;
	}
	
	public String getHTML(){
		String result="<tr><td>"+getName()+"</td><td>"+getP1()+"</td><td>"+getP2()+"</td><td>"+getP3()+"</td><td>"+getP4()+"</td></tr>";
		return result;
	}
	
	public String toString(){
		String result=getName()+":"+getP1()+":"+getP2()+":"+getP3()+":"+getP4()+":"+getBoard();
		return result;
	}
}
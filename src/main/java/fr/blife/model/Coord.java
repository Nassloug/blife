package fr.blife.model;

public class Coord {

	private int x;
	private int y;

	  public Coord(int x, int y) {
	     this.x=x;
	     this.y=y;
	  }

	public int[]getXY(){
		return new int[]{x,y};
	}
	  
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public boolean isNear(Coord coord){
		if(this.equals(coord)){
			return false;
		}
		else{
			int x2 = coord.getX();
			int y2 = coord.getY();
			if(x2==x-1){
				if(y2==y-1||y2==y||y2==y+1){
					return true;
				}
			}
			else if(x2==x){
				if(y2==y-1||y2==y+1){
					return true;
				}
			}
			else if(x2==x+1){
				if(y2==y-1||y2==y||y2==y+1){
					return true;
				}
			}
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Coord other = (Coord) obj;
		if (x != other.getX())
			return false;
		if (y != other.getY())
			return false;
		return true;
	}

	  
}
package fr.blife.model;

import java.util.Collection;
import java.util.HashMap;

public class Plateau {
	private HashMap<Coord,Cellule> plateau;
	
	private int nbLigne,nbColonne;
	private int[] sizeTeam;
	
	public Plateau(int nbLigne,int nbColonne, int nbEquipe){
		plateau = new HashMap<Coord,Cellule>();
		this.nbLigne=nbLigne;
		this.nbColonne=nbColonne;
		this.sizeTeam=new int[nbEquipe];
	}
	
	
	public void ajouterCellule(Cellule cellule){
		if(valable(cellule)){
			plateau.put(cellule.getCoord(), cellule);
			sizeTeam[cellule.getEquipe()]++;
		}
	}
	
	private boolean valable(Cellule c){
		int[] coord = c.getCoord().getXY();
		if(coord.length!=2){
			return false;
		}else if(coord[0]>nbLigne || coord[0]<0){
			return false;
		}else if (coord[1]>nbColonne || coord[1]<0){
			return false;
		}else if(c.getEquipe()>sizeTeam.length){
			return false;
		}else if(c.getEquipe()<0){
			return false;
		}
		return true;
	}
	
	public Cellule getCellule(int x, int y){
		
		return plateau.get(new Coord(x, y));
	}
	
	public Cellule getCellule(Coord c){
		if(plateau.get(c)==null)ajouterCellule(new Cellule(c.getX(),c.getY(),0));
		return plateau.get(c);
	}
	
	public void removeCellule(int x,int y){
		Cellule c =plateau.remove(new Coord(x, y));
		sizeTeam[c.getEquipe()]--;
	}
	
	public void removeCellule(Coord c){
		Cellule cellule =plateau.remove(c);
		sizeTeam[cellule.getEquipe()]--;
	}
	
	public void changeEquipe(Cellule c,int equipe){
		sizeTeam[c.getEquipe()]--;
		c.setEquipe(equipe);
		sizeTeam[c.getEquipe()]++;
	}
	
	public int getSize(){
		return plateau.size();
	}
	
	public Collection<Cellule> getCellules(){
		return plateau.values();
	}
	
	public int getSizeTeam(int equipe){
		if(equipe>=0 && equipe<sizeTeam.length){
			return sizeTeam[equipe];
		}
		return -1;
	}
	
	public int getNbLignes(){
		return nbLigne;
	}
	
	public int getNbColonnes(){
		return nbColonne;
	}
	
	public boolean isIn(int x, int y){
		return plateau.containsKey(new Coord(x,y));
	}
	
	public void getMapFromString(String text){
		for(int i=0;i<text.length();i++){
			if(text.charAt(i)!=0){
				ajouterCellule(new Cellule((int)Math.ceil(i/nbLigne),i%nbColonne,text.charAt(i)));
			}
		}
	}
	
	public String toString(){
		String result = "";
		for(int i=0; i<nbLigne; i++){
			for(int j=0; j<nbColonne; j++){
				if(isIn(i,j)){
					result += ""+getCellule(i,j).getEquipe();
				}
				else{
					result += ""+0;
				}
			}
		}
		return result;
	}
}

package fr.iutinfo;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import javax.ws.rs.core.Application;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;

import fr.blife.App;
import fr.blife.model.User;
import fr.blife.ressources.InscriptionRessource;
import fr.blife.utils.Requests;


public class InscriptionTest extends JerseyTest {

	@Override
    protected Application configure() {
        return new App();
    }
	
	@Test
	public void testRegister() {
		InscriptionRessource res = new InscriptionRessource();
		User u = new User("toto", "toto");
	
		
		User u2 = new User("titi", "toto");
		
		
		
		assertTrue(res.register(u).getSuccess());
		assertFalse(res.register(u).getSuccess());
		assertFalse(res.register(u2).getSuccess());
		Requests r = new Requests();
		r.supprimerCompte(u);
	}
}
